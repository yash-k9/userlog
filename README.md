# UserLog App

An Android application to log the user details to Google Sheets.

[__APK__](https://github.com/yash-k9/UserLog/blob/master/UserLog.apk)

[__DriveLink__](https://docs.google.com/spreadsheets/d/190wwDnCEbDHu3VsAtZsgaIo6XMcWyMJr7gnFjCjKrak/edit?usp=sharing)
